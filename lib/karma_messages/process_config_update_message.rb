module Karma::Messages

  class ProcessConfigUpdateMessage < BaseKarmap

    attr_reader :memory_max, :cpu_quota, :min_running, :max_running, :auto_restart, :auto_start, :push_notifications, :log_level, :num_threads, :notify_interval

    def parse(params)
      super(params)

      @errors << { min_running:   'Required' } if params[:min_running].nil?
      @errors << { max_running:   'Required' } if params[:max_running].nil?
      @errors << { auto_restart:  'Required' } if params[:auto_restart].nil?
      @errors << { auto_start:    'Required' } if params[:auto_start].nil?
      @errors << { log_level:     'Required' } if params[:log_level].nil?
      @errors << { num_threads:   'Required' } if params[:num_threads].nil?

      @memory_max           = params[:memory_max]
      @cpu_quota            = params[:cpu_quota]
      @min_running          = params[:min_running]
      @max_running          = params[:max_running]
      @auto_restart         = params[:auto_restart]
      @auto_start           = params[:auto_start]
      @push_notifications   = params[:push_notifications]
      @log_level            = params[:log_level]
      @num_threads          = params[:num_threads]
      @notify_interval      = params[:notify_interval]
    end

    def to_message
      ret = super
      ret.merge!({ min_running: @min_running, max_running: @max_running, auto_restart: @auto_restart, auto_start: @auto_start })
      ret.merge!({ log_level: @log_level, num_threads: @num_threads })
      ret.merge!({ memory_max: @memory_max} ) if @memory_max.present?
      ret.merge!({ cpu_quota: @cpu_quota} ) if @cpu_quota.present?
      ret.merge!({ push_notifications: @push_notifications} ) if @push_notifications.present?
      ret.merge!({ notify_interval: @notify_interval} ) if @notify_interval.present?
      ret
    end

    def to_config
      {
        memory_max: memory_max,
        cpu_quota: cpu_quota,
        min_running: min_running,
        max_running: max_running,
        auto_restart: auto_restart,
        auto_start: auto_start,
        push_notifications: push_notifications,
        num_threads: num_threads,
        log_level: log_level,
        notify_interval: notify_interval
      }
    end

  end

end
