module Karma::Messages

  class ProcessCommandMessage < BaseKarmap

    attr_reader :command, :pid

    COMMANDS = {start: 'start', stop: 'stop', restart: 'restart'}.freeze

    def self.services=(val)
      @@services = val
    end

    def self.services
      @@services
    end

    def initialize(params)
      super(params)
    end

    def parse(params)
      super(params)
      @@services ||= {}
      @errors << {service: 'Not a valid service'} if !@@services.empty? && !@@services.include?(params[:service])
      @errors << {command: 'Required'} if params[:command].nil?
      @errors << {command: 'Not a valid command'} if !params[:command].nil? && !COMMANDS.values.include?(params[:command])
      @errors << {pid: 'Required'} if params[:command] == 'stop' && params[:pid].nil?

      @command = params[:command]
      @pid = params[:pid]
    end

    def to_message
      ret = super
      ret.merge!({ type: message_type, command: @command})
      ret.merge!({ pid: @pid}) if @command != COMMANDS[:start]
      ret
    end

  end

end
