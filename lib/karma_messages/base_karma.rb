module Karma::Messages

  class BaseKarma < Base

    attr_reader :host, :project

    def parse(params)
      super(params)
      @errors << {host: 'Required'} if params[:host].nil?
      @errors << {project: 'Required'} if params[:project].nil?

      @project = params[:project]
      @host = params[:host]
    end

    def to_message
      ret = super
      ret.merge!({ project: @project, host: @host })
    end

  end

end
