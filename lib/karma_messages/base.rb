module Karma::Messages

  class Base

    attr_accessor :errors, :service

    def initialize(params)
      Karma::Messages.logger.debug("#{self.class.name} - #{params}")
      parse(params)
      Karma::Messages.logger.warn("Message is invalid: - #{errors}") if errors.any?
    end

    def to_message
      { service: @service, type: message_type }
    end

    def parse(params)
      clear_errors
      @errors << {service: 'Required'} if params[:service].nil?
      @service = params[:service]
    end

    def clear_errors
      @errors = []
    end

    def valid?
      @errors.empty?
    end

    def message_type
      self.class.name
    end

  end

end

require "karma_messages/base_karma"
require "karma_messages/base_karmap"

require "karma_messages/process_command_message"
require "karma_messages/process_register_message"
require "karma_messages/process_config_update_message"
require "karma_messages/process_status_update_message"
