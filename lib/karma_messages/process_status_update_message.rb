module Karma::Messages

  class ProcessStatusUpdateMessage < BaseKarma

    STATUSES = {running: 'running', stopped: 'stopped', dead: 'dead'}.freeze

    attr_reader :pid, :status, :active_threads, :execution_time, :performance, :current_version, :cpu, :memory

    def parse(params)
      super(params)
      @errors << {pid: 'Required'} if params[:pid].nil?
      @errors << {status: 'Not a valid status'} if params[:status].nil? && !STATUSES.values.include?(params[:status])

      @pid = params[:pid]
      @status = params[:status]
      @active_threads = params[:active_threads]
      @execution_time = params[:execution_time]
      @performance = params[:performance]
      @current_version = params[:current_version]
      @cpu = params[:cpu]
      @memory = params[:memory]
    end

    def to_message
      ret = super
      ret.merge!({ pid: @pid, status: @status, active_threads: @active_threads, execution_time: @execution_time, performance: @performance, current_version: @current_version, cpu: @cpu, memory: @memory })
    end

  end
end
