require 'logger'

module Karma
  module Messages
    class << self
      attr_writer :logger
      def logger
        @logger ||= Logger.new($stdout, 2, 50*1024*1024).tap do |log|
          log.level = Logger::INFO
          log.progname = self.name
        end
      end
    end
  end
end

require 'karma_messages/version'
require 'karma_messages/base'
